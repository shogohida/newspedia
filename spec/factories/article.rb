FactoryBot.define do
  factory :article, class: Article do
    name { 'Article' }
    content { '法務大臣逮捕' }
  end
end
